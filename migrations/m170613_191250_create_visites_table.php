<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visites`.
 */
class m170613_191250_create_visites_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('visites', [
            'id' => $this->primaryKey(),
            'patient_id' => $this->integer(),
            'date_visite' => $this->date(),
            'motif' => $this->string(),
            'interogatoire' => $this->string(),
            'examen_clinique' => $this->string(),
            'examen_biologique' => $this->string(),
            'examen_radiologique' => $this->string(),
            'exploration_fonctionnelle' => $this->string(),
            'diagnostic' => $this->string(),
            'prevention' => $this->string(),
            'prescription' => $this->string(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-visites-patient_id',
            'visites',
            'patient_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-visites-patient_id',
            'visites',
            'patient_id',
            'patients',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
          // drops foreign key for table `patients`
         $this->dropForeignKey(
             'fk-visites-patient_id',
             'patients'
         );

         // drops index for column `patient_id`
         $this->dropIndex(
             'idx-visites-patient_id',
             'patients'
         );
        $this->dropTable('visites');
    }
}
