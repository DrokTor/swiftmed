<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Pjax;

AppAsset::register($this);
\conquer\momentjs\MomentjsAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => '<img src="images/logo.png" class="img-responsive col-md-4"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Liste des Patients','linkOptions'=>['class'=>'ajax-request'], 'url' => ['/site/index-ajax']],
            ['label' => 'Nouveau Patient','linkOptions'=>['class'=>'ajax-request'], 'url' => ['/site/create-ajax']],
            //['label' => 'Visites', 'url' => ['/site/about']],


        ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?php /* Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */?>
        <?= $content ?>
    </div>
    <div id="loading" class="collapse"><div><img src="images/loading.svg"/></div></div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><a href="http://www.optidev-dz.com">&copy; optidev</a> <?= date('Y') ?></p>

     </div>
</footer>
<?php $this->endBody() ?>
</body>

<script>
  $(document).ready(function() {
$(document).on('click','#historique .visite', function(event) {
  //console.log($(event.target).data('value'));
  $('.visite_heading').text('Date de la visite:');
  $('.selected').toggleClass('selected');
  $(event.target).toggleClass('selected');
  val= $(event.target).data('value');

  if($('#new-visit-container').length)
  $('#new-visit-container').fadeOut(500,function(){
    $('#visit-container').fadeIn(500);

  });
  //else $('#new-visit-container').fadeIn(500);

  moment.locale('fr');

  $.post('/visites/visit', {id: val}, function(data, textStatus, xhr) {
    date=moment(data.date_visite).format("Do MMM YYYY");

    $('#date-visite').fadeOut(500, function() {
        $(this).text(date).fadeIn(500);


    });

    $('#print-link').fadeOut(500, function() {
        $(this).attr('href',"ordonnance?id="+val).fadeIn(500);
    });

    $('#motif_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.motif).fadeIn(500);
    });
    $('#interrogatoire_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.interogatoire).fadeIn(500);
    });
    $('#exam_cli_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.examen_clinique).fadeIn(500);
    });
    $('#exam_bio_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.examen_biologique).fadeIn(500);
    });
    $('#exam_radio_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.examen_radiologique).fadeIn(500);
    });
    $('#exploration_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.exploration_fonctionnelle).fadeIn(500);
    });

    $('#prevention_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.prevention).fadeIn(500);
    });

    $('#diagnostic_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.diagnostic).fadeIn(500);
    });
    $('#prescription_record').fadeOut(500, function() {
        $(this).text('');
        $(this).text(data.prescription).fadeIn(500);
    });

  });
});

$(document).on('click','#new-visit', function(event) {

  moment.locale('fr');
  date=moment().format("Do MMM YYYY");

  if($('#visit-container').length)
  $('#visit-container').fadeOut(500,function(){
    $('#new-visit-container').fadeIn(500);

  });
  else $('#new-visit-container').fadeIn(500);



  $('#date-visite').fadeOut(500, function() {
      $(this).text(date).fadeIn(500);
  });
  // $('#diagnostic').fadeOut(500, function() {
  //     $(this).html('<textarea class="col-md-12"></textarea>').fadeIn(500);
  // });
  // $('#prescription').fadeOut(500, function() {
  //     $(this).html('<textarea class="col-md-12"></textarea>').fadeIn(500);
  // });
  $('#save-visit').fadeIn(500);
});


$(document).on('click','#save-visit', function(event) {
  event.preventDefault();

   answer=confirm("êtes-vous sûr de vouloir clôturer la visite ?");


  if(!answer) return false;
  id=$('#id-patient').text();
  date=$('#date-visite').text();
  //diag=$('#diagnostic').children('textarea')[0].value;
  //pres=$('#prescription').children('textarea')[0].value;
  form= $('#new-visit-container').serializeArray()
  //console.log(form);
  $.post('/visites/save-visit', {id:id, date: date, form:form }, function(data, textStatus, xhr) {

    $('#new-visit-container').fadeOut(500,function(){
      $('#visit-container').fadeIn(500);
      $('#new-visit-container textarea').each(function(index, el) {
        el.value='';
        //console.log(el.value);
      });
    });

    $('#date-visite').fadeOut(500, function() {
        $(this).text(date).fadeIn(500);
    });

    $('#motif_record').fadeOut(500, function() {
        $(this).text(form[0].value).fadeIn(500);
    });
    $('#interrogatoire_record').fadeOut(500, function() {
        $(this).text(form[1].value).fadeIn(500);
    });
    $('#exam_cli_record').fadeOut(500, function() {
        $(this).text(form[2].value).fadeIn(500);
    });
    $('#exam_bio_record').fadeOut(500, function() {
        $(this).text(form[3].value).fadeIn(500);
    });
    $('#exam_radio_record').fadeOut(500, function() {
        $(this).text(form[4].value).fadeIn(500);
    });
    $('#exploration_record').fadeOut(500, function() {
        $(this).text(form[5].value).fadeIn(500);
    });
    $('#diagnostic_record').fadeOut(500, function() {
        $(this).text(form[6].value).fadeIn(500);
    });
    $('#prevention_record').fadeOut(500, function() {
        $(this).text(form[7].value).fadeIn(500);
    });
    $('#prescription_record').fadeOut(500, function() {
        $(this).text(form[8].value).fadeIn(500);
    });


    newval= ($('.visite').length)?parseInt($('.visite')[0].dataset.value,10)+1:0;
    //console.log(newval);
    $('.selected').toggleClass('selected');
    $("#historique>ul").prepend('<li class="visite selected" data-value="'+newval+'">'+date+'</li>');
    $('.selected').fadeOut(400,function(){
        $('.selected').fadeIn(400);
    });
    $('#print-link').fadeOut(500, function() {
        $(this).attr('href',"ordonnance?id="+newval).fadeIn(500);
    });

  });
});








    $('#welcome').fadeIn('fast', function() {
      $('#welcome h1').fadeIn(1000,function(){

          $(this).animate({
                  'margin-top': '50px'
                }, 'medium', function() {
                  // Animation complete.
                  $('#welcome h2').fadeIn(400, function(){
                    setTimeout(function() {
                      $('#welcome').fadeOut();
                    }, 1000);
                  });
              });
      });
    });

    $(document).on('change','#next-visit', function(event) {
      val=event.target.value
      console.log(val);
      id=$('#id-patient').text();
      console.log(id);

      $.post('/next-visit', {id:id,date: val}, function(data, textStatus, xhr) {
        if(data=='true') $('#saved').fadeOut('500', function() {
          $(this).text('Date enregistrée.').fadeIn(500, function() {

          }).delay(3000).fadeOut('500');
        });
      });


  });

    $(".dataTable.no-footer").attr({style: '' });
    $(".patients-index").fadeIn(400);
    $(".patients-form").fadeIn(400);
    $(".patients-view").fadeIn(400);

    $(document).on('click','.ajax-request', function(event) {
      event.preventDefault();

      $.post($(event.target).attr('href'), function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });

    $(document).on('click','.ajax-request-form', function(event) {
      event.preventDefault();
      post=$('.patients-form form').serializeArray();
      //console.log(post);
      $.post($(event.target).attr('href'), post, function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });

    $( document ).ajaxStart(function() {
      $( "#loading" ).fadeIn();
    });

    $( document ).ajaxStop(function() {
      $( "#loading" ).fadeOut();
    });

  });

  $(document).ajaxComplete(function(event) {
    //console.log(event.target);
    $(".dataTable.no-footer").attr({style: '' });
    $(".patients-index").fadeIn(400);
    $(".patients-form").fadeIn(400);
    $(".patients-view").fadeIn(400);
/*
    $(document).on('click','.ajax-request', function(event) {
      event.preventDefault();

      $.post($(event.target).attr('href'), function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });
    $(document).on('click','.ajax-request-form', function(event) {
      event.preventDefault();
      post=$('.patients-form form').serializeArray();
      //console.log(post);
      $.post($(event.target).attr('href'), post, function(data, textStatus, xhr) {

        $('.wrap > .container').html(data);

        //$(".patients-create").fadeIn(400);

      });

    });

/*
    $( document ).ajaxStart(function() {
      $( "#loading" ).show();
    });
    $( document ).ajaxStop(function() {
      $( "#loading" ).hide();
    });*/
  });

</script>
</html>
<?php $this->endPage() ?>
