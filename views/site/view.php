<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Patients */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;
?>

<div class="col-md-3">
<h3>Historique du Patient</h3>
<div  id="historique">
<ul>
<?php

  foreach ($visites as $key => $value) {
    echo '<li class="visite '.(($key=='0')?'selected':$key).'" data-value="'.$value->id.'">'.$formatter->asDate($value->date_visite,'long')."</li>";
  }

 ?>
</ul>
</div></div>
<div class="patients-view col-md-9 collapse">


    <div class="row">
    <p class="col-md-5">
        <?= Html::a('Modifier les informations du patient', ['update-ajax', 'id' => $model->id], ['class' => 'btn btn-primary ajax-request']) ?>
        <?php /* Html::a('Supprimer', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'êtes vous sûr de vouloir supprimer ce Patient?',
                'method' => 'post',
            ],
        ]) */?>
    </p>
    <p class="text-primary col-md-4">
      Prochaine visite programmée pour le :
    </p>
    <p class="next-visit-container col-md-3">
    <?= yii\jui\DatePicker::widget(['name' => 'next_visit','value' => ($model->next_visit)?$model->next_visit:'','language'=>'fr_FR','dateFormat' => 'long','id'=>'next-visit']) ?>
    <br><span id="saved" class="text-success">&nbsp</span>
    </p>
  </div>
    <hr>
    <?php

      echo '<div class="container-fluid"><div class="col-md-3"><label>Identifiant: </label><br>
      <label>Genre: </label>
      </div>';

      echo '<div class="info col-md-3">
        <span id="id-patient">'.$model->id.'</span><br>
        <span>'.$model->sex->nom.' ( '.$age.' )</span><br></div>';

      echo '<div class="col-md-3">
        <label>Nom: </label><br>
        <label>Prénom: </label><br>
        </div>';

        echo '<div class="info col-md-3">
          <span>'.$model->nom.'</span><br>
          <span>'.$model->prenom.'</span><br></div></div>';

      echo ' <div class="row pull-right" data-toggle="collapse" data-target="#more"><a href="#">Voir plus</a></div>
            <br>
            <div id="more" class=" collapse">';

      echo '<br><div class="container-fluid "><div class="col-md-3">
      <label>Tel: </label><br>

      </div>';

      echo '<div class="info col-md-3">
        <span>'.$model->tel.'</span><br>
        </div>';

        echo '<div class="col-md-3">
        <label>Profession: </label><br>

        </div>';

      echo '<div class="info col-md-3">
        <span>'.$model->profession.'</span><br></div>

        </div>';

      echo '<br><div class="container-fluid "><div class="col-md-3">
      <label>Ville: </label><br>

      </div>';

      echo '<div class="info col-md-3">
        <span>'.$model->wilaya->nom.'</span><br>
         </div>';



        echo '<div class="col-md-3">
        <label>Adresse: </label><br>

        </div>';
      echo '<div class="info col-md-3">
        <span>'.$model->adresse.'</span><br></div>
        </div>';

        echo '<br><div class="container-fluid "><div class="col-md-3">
        <label>Antécedents personnels:</label><br>

        </div>';

        echo '<div class="info col-md-3">
          <span>'.$model->antecedents_per.'</span><br>
           </div> ';

        echo ' <div class="col-md-3">
         <label>Antécedents familiaux: </label><br>

        </div>';

        echo '<div class="info col-md-3">
           <span>'.$model->antecedents_fam.'</span><br>
          </div></div>';

        echo '<br><div class="container-fluid "><div class="col-md-3">
        <label>Note: </label><br>

        </div>';
      echo '<div class="info col-md-9">
        <span>'.$model->description.'</span><br>
        </div></div>
        </div>';

     ?>

     <hr>
     <div class="container-fluid">
       <h4  >
         <span class="visite_heading" >Date de la dernière visite:</span>
          &nbsp <label id="date-visite" class="text-success"> &nbsp
            <?php  echo isset($visites[0])? $formatter->asDate($visites[0]->date_visite,'long'):' - Historique du patient vide.'; ?>
          </label>
          <a id="print-link" target="_blank" href="ordonnance?id=<?php if(isset($visites[0])) echo ($visites[0]['id']) ; ?>" <?php if(!isset($visites['0'])) { echo 'class="collapse"'; } ?> ><i class=" print pull-right glyphicon glyphicon-print"></i>
          </a>
        </h4>
        <button id="new-visit" class="btn btn-primary pull-right">Nouvelle visite</button>
       </div>
       <?php if(isset($visites['0'])) { ?>
     <div id="visit-container" class="row">

       <div class="row container-fluid">
       <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
         <p id="motif_record" class="old-visits  col-md-12">
           <?= $visites['0']->motif ?>
         </p>
       </div>
       <div  class="col-md-6"><label class="col-md-12">Interrogatoire:</label><br>
         <p id="interrogatoire_record" class="old-visits  col-md-12">
           <?= $visites['0']->interogatoire ?>
         </p>
       </div>
       </div>
       <div class="row container-fluid">
       <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
         <p id="exam_cli_record" class="old-visits  col-md-12">
           <?= $visites['0']->examen_clinique ?>
         </p>
       </div>
       <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
         <p id="exam_bio_record" class="old-visits  col-md-12">
           <?= $visites['0']->examen_biologique ?>
         </p>
       </div>
      </div>
      <div class="row container-fluid">
       <div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
         <p id="exam_radio_record" class="old-visits  col-md-12">
           <?= $visites['0']->examen_radiologique ?>
         </p>
       </div>
       <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
         <p id="exploration_record" class="old-visits  col-md-12">
           <?= $visites['0']->exploration_fonctionnelle ?>
         </p>
        </div>
        </div>
        <div class="row container-fluid">
       <div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
         <p id="diagnostic_record" class="old-visits  col-md-12">
           <?= $visites['0']->diagnostic ?>
         </p>
       </div>
       <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
         <p id="prevention_record" class="old-visits  col-md-12">
           <?= $visites['0']->prevention ?>
         </p>
        </div>
        </div>
        <div class="row container-fluid">
        <div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
          <p id="prescription_record" class="old-visits  col-md-12">
            <?= $visites['0']->prescription ?>
          </p>
         </div>
        </div>
        </div>
        <?php }else{ ?>
          <div id="visit-container" class="row collapse">
            <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
              <p id="motif_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-6"><label class="col-md-12">Interrogatoire:</label><br>
              <p id="interrogatoire_record" class="old-visits  col-md-12">
               </p>
             </div>
             </div>
             <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
              <p id="exam_cli_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
              <p id="exam_bio_record" class="old-visits  col-md-12">
               </p>
             </div>
           </div>
           <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
              <p id="exam_radio_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
              <p id="exploration_record" class="old-visits  col-md-12">
               </p>
             </div>
           </div>
           <div class="row container-fluid">
            <div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
              <p id="diagnostic_record" class="old-visits  col-md-12">
               </p>
            </div>
            <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
              <p id="prevention_record" class="old-visits  col-md-12">
               </p>
             </div>
           </div>
           <div class="row container-fluid">
             <div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
               <p id="prescription_record" class="old-visits  col-md-12">
                </p>
              </div>
            </div>
            </div>
        <?php } ?>


        <!-- visit form collaped -->
        <form id="new-visit-container"   class="row collapse">

          <div class="col-md-6"><label class="col-md-12">Motif:</label><br>
            <p id="motif" class="new-visits  col-md-12">
              <textarea name="motif" class="col-md-12"></textarea>
            </p>
          </div>
          <div  class="col-md-6"><label class="col-md-12">Interrogatoire:</label><br>
            <p id="interrogatoire" class="new-visits  col-md-12">
              <textarea name="interogatoire" class="col-md-12"></textarea>
            </p>
           </div>
          <div class="col-md-6"><label class="col-md-12">Examen clinique:</label><br>
            <p id="examen-cli" class="new-visits  col-md-12">
              <textarea name="examen_clinique" class="col-md-12"></textarea>
            </p>
          </div>
          <div  class="col-md-6"><label class="col-md-12">Examen biologique:</label><br>
            <p id="examen-bio" class="new-visits  col-md-12">
              <textarea name="examen_biologique" class="col-md-12"></textarea>
            </p>
           </div>
          <div class="col-md-6"><label class="col-md-12">Examen radiologique:</label><br>
            <p id="examen-rad" class="new-visits  col-md-12">
              <textarea name="examen_radiologique" class="col-md-12"></textarea>
            </p>
          </div>
          <div  class="col-md-6"><label class="col-md-12">Exploration fonctionnelle :</label><br>
            <p id="exploration" class="new-visits  col-md-12">
              <textarea name="exploration_fonctionnelle" class="col-md-12"></textarea>
            </p>
           </div>
          <div class="col-md-6"><label class="col-md-12">Diagnostic:</label><br>
            <p id="diagnostic" class="new-visits  col-md-12">
              <textarea name="diagnostic" class="col-md-12"></textarea>
            </p>
          </div>
          <div  class="col-md-6"><label class="col-md-12">Prévention:</label><br>
            <p id="prevention" class="new-visits  col-md-12">
              <textarea name="prevention" class="col-md-12"></textarea>
            </p>
           </div>
          <div  class="col-md-6"><label class="col-md-12">Traitement:</label><br>
            <p id="prescription" class="new-visits  col-md-12">
              <textarea name="prescription" class="col-md-12"></textarea>
            </p>
           </div>
           <button id="save-visit" class=" collapse btn btn-success pull-right"   >Enregistrer la visite</button>
         </form>

      <!-- visit form -->

</div>
