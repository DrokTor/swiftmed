<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PatientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Liste des Patients';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class=" collapse patients-index">

    <h1 class="row "><?= Html::encode($this->title) ?> </h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="col-md-2 pull-right">
        <?= Html::a('Nouveau Patient', ['/site/create-ajax'], ['class' => 'ajax-request btn btn-success']) ?>
    </p>



<?= \nullref\datatable\DataTable::widget([
    'data' => $dataProvider->getModels(),
    'id'=>'datatable-id',
    /*'serverSide' => true,
    'ajax' => '/site/index-json',*/
    'language'=>["sProcessing"=>"Traitement en cours ...",
    "sLengthMenu"=>"Afficher _MENU_ lignes",
    "sZeroRecords"=>"Aucun résultat trouvé",
    "sEmptyTable"=>"Aucune donnée disponible",
    "sInfo"=>"Lignes _START_ à _END_ sur _TOTAL_",
    "sInfoEmpty"=>"Aucune ligne affichée",
    "sInfoFiltered"=>"(Filtrer un maximum de_MAX_)",
    "sInfoPostFix"=>"",
    "sSearch"=>"Chercher:",
    "sUrl"=>"",
    "sInfoThousands"=>",",
    "sLoadingRecords"=>"Chargement...",
    "oPaginate"=>[
      "sFirst"=>"Premier", "sLast"=>"Dernier", "sNext"=>"Suivant", "sPrevious"=>"Précédent"
    ],
    "oAria"=>[
      "sSortAscending"=>": Trier par ordre croissant", "sSortDescending"=>": Trier par ordre décroissant"
    ]],
    'width' =>'900px',
    'columns' => [
        'id',
        'nom',
        'prenom',
        'naissance',
        'ville',

        [
            'class' => 'nullref\datatable\LinkColumn',
            'url' => ['/view-ajax'],
            'options' => [   'class'=>'ajax-request'],
            'queryParams' => ['id'],
            'label' => 'Inspecter <i class="glyphicon glyphicon-search"></i>',
        ],
    ],
]) ?>


</div>

<?php

$session=Yii::$app->session;
$session->open();
if(!$session->get('new'))
{$session->set('new', 'true');
 ?>

<div id="welcome" class="collapse"><div><h1 class="collapse">Bienvenue dans <img src="images/logo.png" /> </h1><h2 class="collapse">Votre système de gestion des Patients.</h2></div></div>

<?php
}
?>
