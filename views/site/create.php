<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Patients */

$this->title = 'Nouveau Patient';
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patients-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'genres' => $genres,
        'wilayas' => $wilayas
    ]) ?>

</div>
