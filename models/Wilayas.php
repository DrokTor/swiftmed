<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wilayas".
 *
 * @property integer $code
 * @property string $nom
 */
class Wilayas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wilayas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'nom'], 'required'],
            [['code'], 'integer'],
            [['nom'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'nom' => 'Nom',
        ];
    }
}
