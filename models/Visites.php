<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visites".
 *
 * @property integer $id
 * @property integer $patient_id
 * @property string $date_visite
 * @property string $diagnostic
 * @property string $prescription
 *
 * @property Patients $patient
 */
class Visites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id'], 'integer'],
            [['date_visite'], 'safe'],
            [['diagnostic', 'prescription','motif','interogatoire','examen_clinique','examen_biologique','examen_radiologique','exploration_fonctionnelle','prevention'], 'string', 'max' => 255],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patients::className(), 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'date_visite' => 'Date Visite',
            'diagnostic' => 'Diagnostic',
            'prescription' => 'Prescription',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patients::className(), ['id' => 'patient_id']);
    }
}
