<?php

namespace app\controllers;

use Yii;
use app\models\Patients;
use app\models\Genres;
use app\models\Wilayas;
use app\models\Visites;
use app\models\PatientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use kartik\mpdf\Pdf;


/**
 * SiteController implements the CRUD actions for Patients model.
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
      \Yii::$app->language = 'fr_FR';
      Parent::init();
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Patients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,['wilaya']);
        //dd($dataProvider->getModels());
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndexAjax()
    {
      $searchModel = new PatientsSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams,['wilaya']);
      //dd($dataProvider->getModels());
      return $this->renderAjax('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
    }



    public function actionDatatables()
    {
        return [
             'datatables' => [
                 'class' => 'nullref\datatable\DataTableAction',
                 'query' => Patients::find()->with('wilaya'),
                 'applyOrder' => function($query, $columns, $order) {
                    //custom ordering logic
                    return $query;
                 },
                 'applyFilter' => function($query, $columns, $search) {
                    //custom search logic
                    return $query;
                 },
             ],
        ];
    }
    /**
     * Displays a single Patients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $visites= Visites::find()->where(['patient_id' => $id])->orderBy('id DESC')->all();
        $model=$this->findModel($id);

        $age= date_diff(date_create($model->naissance), date_create('today'))->y;

        return $this->render('view', [
            'model' => $model,
            'visites'=> $visites,
            'age'=>$age,
        ]);
    }
    public function actionViewAjax($id)
    {
        $visites= Visites::find()->where(['patient_id' => $id])->orderBy('id DESC')->all();
        $model=$this->findModel($id);
        $age= date_diff(date_create($model->naissance), date_create('today'))->y;

        return $this->renderAjax('view', [
            'model' => $model,
            'visites'=> $visites,
            'age'=>$age,
        ]);
    }

    public function actionOrdonnance($id) {

    $visite= Visites::find()->with('patient')->where(['id'=>$id])->one();
    //dd($visite);
    //$traitement=  Yii::$app->request->post('traitement');
    // get your HTML raw content without any layouts or scripts
    $content = $this->renderPartial('_ordonnance',['visite'=>$visite]);

    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_UTF8,
        // A4 paper format
        'format' => [148,210],//Pdf::FORMAT_LEGAL,
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/bower/bootstrap/dist/css/bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.mrg{margin-right:50px;} div{font-size:15px; display:inline-block; } .bld{font-weight:bold}',
         // set mPDF properties on the fly
        'options' => ['title' => 'ordonnancelogiciel swiftmed'],
         // call mPDF methods on the fly
        'methods' => [
            'SetFooter'=>['générée par le logiciel swiftmed '],
            //'SetFooter'=>['{PAGENO}'],
        ]
    ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    protected function frdatetosql($date)
    {
      //convert frenhc date to date
     $find = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
     $replace = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
     $date = str_replace($find, $replace, strtolower($date));
     $date = date('Y/m/d', strtotime($date));

     return $date;
    }
    public function actionNextVisit()
    {
      if(Yii::$app->request->isAjax)
      {
        $post=Yii::$app->request->Post();
        $patient=Patients::findOne($post['id']);



        //var_dump(date_parse($arr));
        //var_dump(date_parse());

        $patient->next_visit=$this->frdatetosql($post['date']);
        if($patient->save())
        {
          return 'true';
        }else return 'false';
      }
    }
    /**
     * Creates a new Patients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patients();

        $genders = ArrayHelper::map(Genres::find()->all(), 'id', 'nom');
        $Wilayas = ArrayHelper::map(Wilayas::find()->all(), 'code', 'nom');
        $post=Yii::$app->request->post();
        //var_dump($post);die();
        //dd($post);
        if($post)$post['Patients']['naissance']=$this->frdatetosql($post['Patients']['naissance']);
        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
            //return $this->runAction('view-ajax',['id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'genres' => $genders,
                'wilayas' => $Wilayas
            ]);
        }
    }
    public function actionCreateAjax()
    {
        $model = new Patients();

        $genders = ArrayHelper::map(Genres::find()->all(), 'id', 'nom');
        $Wilayas = ArrayHelper::map(Wilayas::find()->all(), 'code', 'nom');
        $post=Yii::$app->request->post();
        //var_dump($post);die();
        //dd($post);


        if($post)$post['Patients']['naissance']=$this->frdatetosql($post['Patients']['naissance']);
        if ($model->load($post) && $model->save()) {
            //return $this->renderAjax('view', ['id' => $model->id]);
            return $this->runAction('view-ajax',['id' => $model->id]);

        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'genres' => $genders,
                'wilayas' => $Wilayas
            ]);
        }
    }

    /**
     * Updates an existing Patients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $genders = ArrayHelper::map(Genres::find()->all(), 'id', 'nom');
        $Wilayas = ArrayHelper::map(Wilayas::find()->all(), 'code', 'nom');
        $post=Yii::$app->request->post();
        if($post)$post['Patients']['naissance']=$this->frdatetosql($post['Patients']['naissance']);

        if ($model->load($post) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'genres' => $genders,
                'wilayas' => $Wilayas
            ]);
        }
    }
    public function actionUpdateAjax($id)
    {
        $model = $this->findModel($id);
        $genders = ArrayHelper::map(Genres::find()->all(), 'id', 'nom');
        $Wilayas = ArrayHelper::map(Wilayas::find()->all(), 'code', 'nom');
        $post=Yii::$app->request->post();
        if($post)$post['Patients']['naissance']=$this->frdatetosql($post['Patients']['naissance']);

        if ($model->load($post) && $model->save()) {
          return $this->runAction('view-ajax',['id' => $model->id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'genres' => $genders,
                'wilayas' => $Wilayas
            ]);
        }
    }

    /**
     * Deletes an existing Patients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Patients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Patients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
